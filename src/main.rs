use std::io::{stdin, IsTerminal};

mod eval;
mod input;
mod prompt;
mod run;
mod types;

use eval::rush_eval;
use input::{input_dumb, input_interactive, reader};
use types::RushEditor;

fn rush_interactive(reader: &mut RushEditor) {
  while let Some(input) = input_interactive(reader) {
    rush_eval(input);
  }
}

fn rush_piped() {
  while let Some(input) = input_dumb() {
    rush_eval(input);
  }
}

fn main() {
  let mut reader = reader();
  match stdin().is_terminal() {
    true => rush_interactive(&mut reader),
    false => rush_piped(),
  }
}
