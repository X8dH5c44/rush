use super::prompt::get as prompt;
use super::types::{Input, RushEditor, RushHistory};
use std::io::stdin;

use rustyline::history::MemHistory;
use rustyline::{Config, Editor};
pub fn reader() -> RushEditor {
  let history = MemHistory::new() as RushHistory;
  let config = Config::builder().auto_add_history(true).build();
  let editor = Editor::with_history(config, history).unwrap();
  editor
}

pub fn input_interactive(reader: &mut RushEditor) -> Option<Input> {
  match reader.readline(prompt().as_str()) {
    Ok(line) => match line.as_str() {
      "" => Some(Input::Empty),
      "\n" => Some(Input::Empty),
      _ => Input::from(line),
    },
    Err(_) => None,
  }
}

pub fn input_dumb() -> Option<Input> {
  let mut line = "".to_string();
  match stdin().read_line(&mut line) {
    Ok(0) => None,
    Ok(1) => Some(Input::Empty),
    Ok(_bytes) => Input::from(line.to_string()),
    Err(_) => None,
  }
}
