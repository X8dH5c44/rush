use rustyline::history::MemHistory;
use rustyline::Editor;
use std::collections::VecDeque;

pub type RushHelper = ();
pub type RushHistory = MemHistory;
pub type RushEditor = Editor<RushHelper, RushHistory>;
pub type ParsedLine = VecDeque<RushCall>;

#[derive(Default)]
pub enum Input {
  Utf8(String),
  #[default]
  Empty,
}

#[derive(Clone)]
pub struct RushCall {
  argv: VecDeque<String>,
}

impl Input {
  pub fn from(input: String) -> Option<Self> { Some(Self::Utf8(input)) }
}

impl RushCall {
  pub fn argc(&self) -> usize { self.argv.len() }
  pub fn argv(&self) -> &VecDeque<String> { &self.argv }
  pub fn argv_mut(&mut self) -> &mut VecDeque<String> { &mut self.argv }
}
