use dirs::home_dir;
use std::env::current_dir;
use std::path::Path;
use whoami::{devicename, username};

pub fn get() -> String {
  let user = user();
  let host = host();
  let wdir = wdir().unwrap_or("<wdir>".to_string());
  format!("{user}@{host}:{wdir}$ ")
}

fn wdir() -> Option<String> {
  match (current_dir(), home_dir()) {
    (Ok(current), Some(home)) => match current.strip_prefix(home) {
      Ok(path) => Some(Path::new("~").join(path).to_string_lossy().to_string()),
      Err(_) => Some(current.to_string_lossy().to_string()),
    },
    _ => None,
  }
}

fn user() -> String { username() }

fn host() -> String { devicename() }
