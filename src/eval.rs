use crate::types::{ParsedLine, RushCall};

use super::run::rush_run;
use super::types::Input;

enum ParseState {
  Start,
  Passthrough,
  CmdSep,
  Stop,
}

struct Parser {
  state: ParseState,
  output: ParsedLine,
}

impl Parser {
  pub fn new() -> Self {
    Self {
      state: ParseState::Start,
      output: ParsedLine::new(),
    }
  }

  pub fn next(&self, char: char) {
    //
  }

  pub fn output(self) -> ParsedLine { self.output }
}

pub fn rush_eval(input: Input) {
  if let Input::Utf8(line) = input {
    let parser = Parser::new();
    for char in line.chars() {
      parser.next(char);
    }
    let output = parser.output();
    rush_run(output);
  }
}
