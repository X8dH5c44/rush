use crate::types::ParsedLine;

use super::types::RushCall;
use std::process::{Command, Output};

pub fn rush_run(calls: ParsedLine) {}

pub fn rush_run_single(mut call: RushCall) -> Option<Output> {
  let argv = call.argv_mut();
  match Command::new(argv.pop_front()?).args(argv).output() {
    Ok(output) => Some(output),
    Err(_) => {
      // TODO: HANDLE ERROR
      None
    },
  }
}
