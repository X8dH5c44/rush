# rush - Rust shell

A shell program written in Rust.

## General

This is a personal project written for exploratory purposes.
As such, it comes with no guarantees whatsoever.
I will, however, do what I can to keep things straightforward and bug-free.

This shell will probably never reach feature-parity with the likes of `bash`,
as this is not my intention.

## Features

* call programs from command line
* `;` operator for chained execution

## TODO

* `&` operator for background execution
* `&&` operator for chained execution on success
* `.` operator to source a shell script
* `.`, `..` and `~` filepath aliases
* `!!` alias for last input
* `cd` builtin to change directory
* `pwd` builtin to show current directory without prompt
* `jobs` builtin to display background jobs
* `exit` builtin to quit the shell
* distinguish between superuser (`#`) and normal user (`$`) at prompt
* prompt settings
  * only simple/complex for now
* variables
  * assignment with `=`
  * reference with `$variable` or `${variable}`
  * `export` functionality
* strings with `""` and `''`
* string interpolation for doubly-quoted (`""`) strings
